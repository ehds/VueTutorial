var app = new Vue({
	el:'#app',
	data:{
		rawHtml:'<span style="color:red">This is red.</span>',
		id:2,
		number:3,
		message:"Hello Vue",
		ok:true,
		url:"http://www.baidu.com"
	},
	methods:{
		doSomething:function(){
			alert(this.message);
		}
	},
	computed:{
		reverseMessage:function(){
			return this.message.split('').reverse().join('');
		}
	}
})


var vm = new Vue({
  el: '#demo',
  data: {
    firstName: 'Foo',
    lastName: 'Bar',
  },
  computed:{
  	fullName:{
  		get:function(){
  			return this.firstName+' '+this.lastName;

  		},
  		set:function(val){
  			var names = newValue.split(' ')
		    this.firstName = names[0]
		    this.lastName = names[names.length - 1]

  		}
  	}
  },
 
})

var appStatic = new Vue({
	el:".static",
	data: {
	  isActive: true,
	  hasError: true,
	  active:"active2"
	}
})
